#! /bin/bash
#cp -v /opensipsfe/opensips_residential.cfg /usr/local/opensips/etc/opensips/opensips_residential.cfg


REDIS_PORT=$(printenv REDIS_SERVICE_PORT)
REDIS_HOST=$(printenv REDIS_SERVICE_HOST)
REST_URL=$(printenv REPO_CLIENT_SERVICE_HOST)
REST_PORT=$(printenv REPO_CLIENT_SERVICE_PORT)
HOST_IP="$( ip route get 8.8.8.8 | awk 'NR==1 {print $NF}' )"
LOG_LEVEL=$(printenv DEBUG_LEVEL)
SCENARIO=$(printenv TEST_SCENARIO)
DUMP_CORE=$(printenv CORE_DUMP)
S_MEM=$(printenv S_MEM)
P_MEM=$(printenv P_MEM)

echo $HOST_IP
echo $REDIS_PORT
echo $REST_URL
echo $REDIS_HOST
echo $REST_PORT
echo $LOG_LEVEL
echo $SCENARIO
echo S_MEM: $S_MEM
echo P_MEM: $P_MEM
cp /usr/share/zoneinfo/EST /etc/localtime

sed -i "s/LOG_LEVEL/$LOG_LEVEL/" /usr/local/opensips/etc/opensips/opensips_residential.cfg
sed -i "s/HOST_IP/$HOST_IP/" /usr/local/opensips/etc/opensips/opensips_residential.cfg
sed -i "s/REDIS_HOST/$REDIS_HOST/" /usr/local/opensips/etc/opensips/opensips_residential.cfg
sed -i "s/REDIS_PORT/$REDIS_PORT/" /usr/local/opensips/etc/opensips/opensips_residential.cfg
sed -i "s/REST_SERVICE/$REST_URL/" /usr/local/opensips/etc/opensips/opensips_residential.cfg
sed -i "s/REST_PORT/$REST_PORT/" /usr/local/opensips/etc/opensips/opensips_residential.cfg
sed -i "s/CORE_DUMP/$DUMP_CORE/" /etc/default/opensips
sed -i "s/S_MEMORY=64/S_MEMORY=$S_MEM/" /etc/default/opensips
sed -i "s/P_MEMORY=4/P_MEMORY=$P_MEM/" /etc/default/opensips

mv /home/opensips /etc/init.d/opensips

cat /etc/default/opensips
echo "****************************************"
cat /etc/init.d/opensips
echo "****************************************"
cat /usr/local/opensips/etc/opensips/opensips_residential.cfg
service opensips restart
#pidstat -ruh -C opensips 5 > "$SCENARIO-"`date +"%M-%S.log"`
