FROM suryaveer/opensips:1.1
MAINTAINER suryaveer
LABEL description="Changed calloc to malloc and 'keys' to 'key' for GET request."
LABEL version="6.0"


COPY Opensips  /opensipsfe/Opensips

RUN cd /opensipsfe/ && cd Opensips && make install \
	&& sed -i "s/debug=7/debug=LOG_LEVEL/" /usr/local/opensips/etc/opensips/opensips_residential.cfg  \
	&& sed -i "s/PresenceRepository\/V1/presence/" /usr/local/opensips/etc/opensips/opensips_residential.cfg && make clean

COPY init.sh /etc/my_init.d/init.sh
## For deploying on local docker
#RUN sed -i "s/REST_SERVICE:REST_PORT/192.168.253.1:8080/" /usr/local/opensips/etc/opensips/opensips_residential.cfg \
#	&& sed -i "s/debug=LOG_LEVEL/debug=7/" /usr/local/opensips/etc/opensips/opensips_residential.cfg  \
#	&& sed -i "s/REDIS_HOST/172.17.0.2/" /usr/local/opensips/etc/opensips/opensips_residential.cfg \
#	&& sed -i "s/REDIS_PORT/6379/" /usr/local/opensips/etc/opensips/opensips_residential.cfg


EXPOSE 5060/udp


# Clean up APT when done.
RUN apt-get clean && rm -rvf /var/lib/apt/lists/* /tmp/* /var/tmp/*